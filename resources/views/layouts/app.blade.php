<!DOCTYPE html>
<html lang="en">
	<head>
		@include('layouts.head')
	</head>
	<body>
		<div class="main-wrapper">
			@include('layouts.header')
			@yield('content')
		</div>

		@include('layouts.footer')
	</body>
</html>