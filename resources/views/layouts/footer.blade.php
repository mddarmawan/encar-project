	<script src="{{ asset('bower_components') }}/jquery/dist/jquery.min.js" type="text/javascript"></script>
	<script src="{{ asset('bower_components') }}/jsgrid/dist/jsgrid.min.js" type="text/javascript"></script>

	<script src="{{ asset('assets') }}/lib/tether/js/tether.min.js" type="text/javascript"></script>
	<script src="{{ asset('assets') }}/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
	<script src="{{ asset('assets') }}/lib/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="{{ asset('assets') }}/js/app.js" type="text/javascript"></script>
	<script src="{{ asset('assets') }}/lib/theme-switcher/theme-switcher.min.js" type="text/javascript"></script>
    <script src="{{ asset('assets') }}/lib/Avgrund-master/js/avgrund.js" type="text/javascript"></script>

	@yield('scripts')

	<script type="text/javascript">
		$(document).ready(function(){
			App.livePreview();
		});
	</script>