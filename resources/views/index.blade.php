@extends('layouts.app')

@section('content')

	<div class="panel-header-encar">
		<h6 class="left">
            <small style="margin-bottom: 40px">Penjualan</small>
            <br>
        </h6>
       	 <h5>SPK</h5>
	</div>

	<div class="content_encar">
		<div class="main-content ">
			<div class="col-sm-12">
				<div class="panel panel-default panel-table">
					<div class="panel-body">
						<div id="jsGrid"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	@section('scripts')
	
		<script type="text/javascript">
			$(document).ready(function(){
				//initialize the javascript
				App.init();

				var clients = [
					{ "Name": "Otto Clay", "Age": 25, "Country": 1, "Address": "Ap #897-1459 Quam Avenue", "Married": false },
					{ "Name": "Connor Johnston", "Age": 45, "Country": 2, "Address": "Ap #370-4647 Dis Av.", "Married": true },
					{ "Name": "Lacey Hess", "Age": 29, "Country": 3, "Address": "Ap #365-8835 Integer St.", "Married": false },
					{ "Name": "Timothy Henson", "Age": 56, "Country": 1, "Address": "911-5143 Luctus Ave", "Married": true },
					{ "Name": "Timothy Henson", "Age": 56, "Country": 1, "Address": "911-5143 Luctus Ave", "Married": true },
					{ "Name": "Timothy Henson", "Age": 56, "Country": 1, "Address": "911-5143 Luctus Ave", "Married": true },
					{ "Name": "Timothy Henson", "Age": 56, "Country": 1, "Address": "911-5143 Luctus Ave", "Married": true },
					{ "Name": "Timothy Henson", "Age": 56, "Country": 1, "Address": "911-5143 Luctus Ave", "Married": true },
					{ "Name": "Timothy Henson", "Age": 56, "Country": 1, "Address": "911-5143 Luctus Ave", "Married": true },
					{ "Name": "Timothy Henson", "Age": 56, "Country": 1, "Address": "911-5143 Luctus Ave", "Married": true },
					{ "Name": "Timothy Henson", "Age": 56, "Country": 1, "Address": "911-5143 Luctus Ave", "Married": true },
					{ "Name": "Timothy Henson", "Age": 56, "Country": 1, "Address": "911-5143 Luctus Ave", "Married": true },
					{ "Name": "Timothy Henson", "Age": 56, "Country": 1, "Address": "911-5143 Luctus Ave", "Married": true },
					{ "Name": "Timothy Henson", "Age": 56, "Country": 1, "Address": "911-5143 Luctus Ave", "Married": true },
					{ "Name": "Timothy Henson", "Age": 56, "Country": 1, "Address": "911-5143 Luctus Ave", "Married": true },
					{ "Name": "Timothy Henson", "Age": 56, "Country": 1, "Address": "911-5143 Luctus Ave", "Married": true },
					{ "Name": "Timothy Henson", "Age": 56, "Country": 1, "Address": "911-5143 Luctus Ave", "Married": true },
					{ "Name": "Timothy Henson", "Age": 56, "Country": 1, "Address": "911-5143 Luctus Ave", "Married": true },
					{ "Name": "Timothy Henson", "Age": 56, "Country": 1, "Address": "911-5143 Luctus Ave", "Married": true },
					{ "Name": "Timothy Henson", "Age": 56, "Country": 1, "Address": "911-5143 Luctus Ave", "Married": true },
					{ "Name": "Timothy Henson", "Age": 56, "Country": 1, "Address": "911-5143 Luctus Ave", "Married": true },
					{ "Name": "Timothy Henson", "Age": 56, "Country": 1, "Address": "911-5143 Luctus Ave", "Married": true },
					{ "Name": "Timothy Henson", "Age": 56, "Country": 1, "Address": "911-5143 Luctus Ave", "Married": true },
					{ "Name": "Timothy Henson", "Age": 56, "Country": 1, "Address": "911-5143 Luctus Ave", "Married": true },
					{ "Name": "Timothy Henson", "Age": 56, "Country": 1, "Address": "911-5143 Luctus Ave", "Married": true },
					{ "Name": "Timothy Henson", "Age": 56, "Country": 1, "Address": "911-5143 Luctus Ave", "Married": true },
					{ "Name": "Timothy Henson", "Age": 56, "Country": 1, "Address": "911-5143 Luctus Ave", "Married": true },
					{ "Name": "Timothy Henson", "Age": 56, "Country": 1, "Address": "911-5143 Luctus Ave", "Married": true },
					{ "Name": "Timothy Henson", "Age": 56, "Country": 1, "Address": "911-5143 Luctus Ave", "Married": true },
					{ "Name": "Timothy Henson", "Age": 56, "Country": 1, "Address": "911-5143 Luctus Ave", "Married": true },
					{ "Name": "Timothy Henson", "Age": 56, "Country": 1, "Address": "911-5143 Luctus Ave", "Married": true },
					{ "Name": "Timothy Henson", "Age": 56, "Country": 1, "Address": "911-5143 Luctus Ave", "Married": true },
					{ "Name": "Timothy Henson", "Age": 56, "Country": 1, "Address": "911-5143 Luctus Ave", "Married": true },
					{ "Name": "Ramona Benton", "Age": 32, "Country": 3, "Address": "Ap #614-689 Vehicula Street", "Married": false }
				];
				 
				var countries = [
					{ Name: "", Id: 0 },
					{ Name: "United States", Id: 1 },
					{ Name: "Canada", Id: 2 },
					{ Name: "United Kingdom", Id: 3 }
				];
			 
				$("#jsGrid").jsGrid({
					width: "100%",
					height: "445px",
			 	
					inserting: true,
					sorting: true,
					paging: true,
					filter: true,

			 
					data: clients,
			 
					fields: [
						{ name: "Name", type: "text", width: 150, validate: "required" },
						{ name: "Age", type: "number", width: 50 },
						{ name: "Address", type: "text", width: 200 },
						{ name: "Country", type: "select", items: countries, valueField: "Id", textField: "Name" },
						{ name: "Married", type: "checkbox", title: "Is Married", sorting: false },
						{ name: "sss", type: "text", title: "Is Married", sorting: false },
						{ name: "seess", type: "text", title: "Is Married", sorting: false },
						{ name: "ssewes", type: "text", title: "Is Married", sorting: false },
						{ name: "sseees", type: "text", title: "Is Married", sorting: false },
						{ name: "saass", type: "text", title: "Is Married", sorting: false },
						{ name: "ff", type: "text", title: "Is Married", sorting: false },						
						{ name: "ssessees", type: "text", title: "Is Married", sorting: false },
						{ name: "saasaaas", type: "text", title: "Is Married", sorting: false },

					]
				});
			});
		</script>

	@endsection

@endsection