@extends('layouts.app')

@section('content')

	@if (!empty(Session::get('message')))
		<script>
			alert('{{ Session::get('message') }}')
		</script>
	@endif

	<div class="panel-header-encar">
		<h6 class="left">
			<small style="margin-bottom: 40px">Master Data</small>
			<br>
		</h6>
		 <h5>Kontak</h5>
	</div>
	<div class="nav-aksi">
		<button onclick="javascript:add();" class="btn-tambah"><i class="icon icon-left s7-plus"> </i> Tambah</button>
	</div>
	<div class="content_encar">
		<div class="main-content ">
			<div class="col-sm-12">
				<div class="panel panel-default panel-table">
					<div class="panel-body">
						<div class="nav-left">
							<ul class="tab" id="kontakTipe">
								<li class="active"><a data-id="" >Semua</a></li>
							</ul>
						</div>
						<div id="dataKontak">

						</div>
					</div> 
				</div>
			</div>
		</div>
	</div>

	<div id="add" class="avgrund-popup" >
		<form id="form" action="/kontak" method="post">
			{{ csrf_field() }}
			<input id="form_method" type="hidden" name="_method" value="put">
			<input id="kontak_id" type="hidden" name="kontak_id">
			<div class="header-modal">
				<a href="#" class="close-modal" onclick="javascript:closeDialog();"><span class="s7-close"></span></a>
				<div class="title-modal">
					Tambah Kontak
					<br>
				</div>
			</div>
			<div class="content-modal">	
				<div class="bingkai">
					<div class="label-bingkai">Kategori Kontak</div>
					<div class="row" style="padding: 10px;">
						<div class="col-md-6" style="display: inline-block;">
							<table class="table-form">
								<tr>
									<td width="140px">Tipe Kontak</td>
									<td width="10px">:</td>
									<td> 
										<select class="form-control custom-select small" id="kontak_tipe" name="kontak_tipe">
						
										</select>
									</td>
								</tr>
							</table>
						</div>
						<div class="col-md-6" style="display: inline-block;">
							<table class="table-form">
								<tr>
									<td width="140px">Kategori Kontak</td>
									<td width="10px">:</td>
									<td> 
										<select class="form-control custom-select small" id="kontak_kategori" name="kontak_kategori" disabled>
						
										</select>
									</td>
								</tr>
							</table>
						</div>
					</div>

				</div>
				<br>
				<div class="bingkai">
					<div class="label-bingkai">Data Kontak</div>
					<div class="row" style="padding: 10px;">
						<div class="col-md-6" style="display: inline-block;">
							<table class="table-form">
								<tr >
									<td width="140px">Nama</td>
									<td>:</td>
									<td><input id="kontak_nama" type="text" placeholder="" name="kontak_nama" class="form-control form-control-xs"></td>
								</tr>
								<tr>
									<td width="140px">Nick</td>
									<td width="10px">:</td>
									<td><input id="kontak_nick" type="text" placeholder="" name="kontak_nick" class="form-control form-control-xs"></td>
								</tr>
								<tr>
									<td width="140px" style="vertical-align: top">Alamat</td>
									<td width="10px" style="vertical-align: top;">:</td>
									<td><textarea id="kontak_alamat" class="form-control" rows="4" name="kontak_alamat"></textarea></td>
								</tr>
							</table>
						</div>
						<div class="col-md-6" style="display: inline-block;">
							<table class="table-form">
								<tr>
									<td width="140px">Kota</td>
									<td>:</td>
									<td><input id="kontak_kota" type="text" placeholder="" name="kontak_kota" class="form-control form-control-xs"></td>
								</tr>
								<tr>
									<td width="140px">Kode Pos</td>
									<td>:</td>
									<td><input id="kontak_pos" type="text" placeholder="" name="kontak_pos" class="form-control form-control-xs"></td>
								</tr>
								<tr>
									<td width="140px">No. Telp</td>
									<td>:</td>
									<td><input id="kontak_telp" type="text" placeholder="" name="kontak_telp" class="form-control form-control-xs"></td>
								</tr>
								<tr>
									<td width="140px">Email</td>
									<td>:</td>
									<td><input id="kontak_email" type="text" placeholder="" name="kontak_email" class="form-control form-control-xs"></td>
								</tr>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="footer-modal">
				<div style="float: right; margin-right: 30px" >
					<button type="submit" name="submit" class="simpan">Simpan</button>	
				</div>
			</div>
		</form>
	</div>

@endsection

@section('scripts')

	<script type="text/javascript">
		App.init();
		var status = "";
		var url = "";

		/*
		 * --------------------------------------------------------------------------
		 * Aksi Filter Data
		 * --------------------------------------------------------------------------
		 */
		$("#filter").click(function(e){
			e.preventDefault();
			loadData();
		});

		/*
		 * --------------------------------------------------------------------------
		 * Aksi Reset Data
		 * --------------------------------------------------------------------------
		 */
		$("#reset").click(function(e){
			e.preventDefault();
			$(".tab li").removeClass("active");
			$(".tab li a[data-id='']").parent().addClass("active");
			loadData();
		});

		/*
		 * --------------------------------------------------------------------------
		 * Fungsi Membersihkan Form
		 * --------------------------------------------------------------------------
		 */
		function clearForm() {	
			$("#kontak_nama").val('');
			$("#kontak_nick").val('');
			$("#kontak_alamat").val('');
			$("#kontak_kota").val('');
			$("#kontak_pos").val('');
			$("#kontak_telp").val('');
			$("#kontak_email").val('');
		}

		/*
		 * --------------------------------------------------------------------------
		 * Fungsi Close Modal
		 * --------------------------------------------------------------------------
		 */
		function closeDialog() {
			Avgrund.hide( "#add" );
		}

		/*
		 * --------------------------------------------------------------------------
		 * Fungsi Tambah Modal
		 * --------------------------------------------------------------------------
		 */
		function add() {
			clearForm();

			$("#form_method").prop("disabled", "disabled");
			$("#kontak_id").prop("disabled", "disabled");
			$("#form").attr("action", "/kontak/");

			listKontakTipe();
			Avgrund.show( "#add" );
		}

		/*
		 * --------------------------------------------------------------------------
		 * Fungsi Modal Edit
		 * --------------------------------------------------------------------------
		 */
		function edit(id) {
			clearForm();

			$("#kontak_kategori").removeAttr("disabled");

			$.ajax({
				type: "GET",
				url: "{{url('api/kontak')}}/" + id
			}).done(function(response){
				var data = response[0];

				listKontakTipe(data.kontak_tipe);
				listKontakKategori(data.kontak_tipe_kode, data.kontak_kategori);

				$("#form").attr("action", "/kontak/" + id);
				$("#kontak_id").val(data.kontak_id);
				$("#kontak_nama").val(data.kontak_nama);
				$("#kontak_nick").val(data.kontak_nick);
				$("#kontak_alamat").val(data.kontak_alamat);
				$("#kontak_kota").val(data.kontak_kota);
				$("#kontak_pos").val(data.kontak_pos);
				$("#kontak_telp").val(data.kontak_telp);
				$("#kontak_email").val(data.kontak_email);
			});
			

			$("#form_method").removeAttr("disabled");
			$("#kontak_id").removeAttr("disabled");
			
			Avgrund.show( "#add" );
		}

		/*
		 * --------------------------------------------------------------------------
		 * Fungsi Load Data Kontak Tipe
		 * --------------------------------------------------------------------------
		 */
		function listKontakTipe(id) {
			$.ajax({
				type: "GET",
				url: "{{url('api/kontakTipe')}}"
			}).done(function(response) {

				/*
				 * --------------------------------------------------------------------------
				 * Assign Value Dropdown Kontak Tipe
				 * --------------------------------------------------------------------------
				 */
				$("#kontak_tipe option").remove();
				$("#kontak_tipe").append("<option value=''>...</option>");
				for (var i in response) {
					$("#kontak_tipe").append("<option data-id='" + response[i].kontak_tipe_kode + "' value='" + response[i].kontak_tipe_id + "'>" + response[i].kontak_tipe_nama + "</option>");
				}
				$("#kontak_tipe").val(id);
			});

			/*
			 * --------------------------------------------------------------------------
			 * Load Data Dropdown Kontak Tipe
			 * --------------------------------------------------------------------------
			 */
			$("#kontak_tipe").on('change', function(){
				if (this.value == '') {
					$("#kontak_kategori option").remove();
					$("#kontak_kategori").prop("disabled", "disabled");
				} else {
					listKontakKategori($(this).find(":selected").data("id"));
				}
			});
		}

		/*
		 * --------------------------------------------------------------------------
		 * Fungsi Load Data Kontak Kategori
		 * --------------------------------------------------------------------------
		 */
		function listKontakKategori(type, id = null) {
			$.ajax({
				type: "GET",
				url: "{{url('api/kontakKategori')}}/" + type
			}).done(function(response) {

				/*
				 * --------------------------------------------------------------------------
				 * Assign Value Dropdown Kontak Kategori
				 * --------------------------------------------------------------------------
				 */
				$("#kontak_kategori").removeAttr("disabled");
				$("#kontak_kategori option").remove();
				$("#kontak_kategori").append("<option value=''>...</option>");

				for (var i in response) {
					$("#kontak_kategori").append("<option value='" + response[i].kontak_kategori_id + "'>" + response[i].kontak_kategori_nama + "</option>");
				}
				$("#kontak_kategori").val(id);
			});
		}

		/*
		 * --------------------------------------------------------------------------
		 * Fungsi Load Tab List Kontak Tipe
		 * --------------------------------------------------------------------------
		 */
		function loadKontakTipe() {
			$.ajax({
				type: "GET",
				url: "http://127.0.0.1:8000/api/kontakTipe"
			}).done(function(response) {
				for (var i in response) {
					$("#kontakTipe").append("<li><a data-id='" + response[i].kontak_tipe_id + "''>" + response[i].kontak_tipe_nama + "</a></li>");
				}

				$("#kontakTipe").append("<li><a data-id='0'>Non Aktif</a></li>");
				$("#kontakTipe").append("<li><a data-id='trash'>Trash</a></li>");
				$(".tab li a").click(function(e){
					e.preventDefault();

					status = $(this).data("id");
					
					loadData();
					$(".tab li").removeClass("active");
					$(this).parent().addClass("active");
				});
			});
		}

		/*
		 * --------------------------------------------------------------------------
		 * Fungsi Utama Load Data
		 * --------------------------------------------------------------------------
		 */
		function loadData() {
			var db = {
				loadData: function(filter) {
					if (status != "") {
						if (status == 0) {
							url = "{{url('api/kontak/inactive')}}";
						} else if (status == "trash") {
							url = "{{url('api/kontak/deleted')}}";
						} else {
							url = "{{url('api/kontak/tipe')}}/" + status;
						}
					} else {
						url = "{{url('api/kontak')}}";
					}

					return $.ajax({
						type: "GET",
						url: url
					});
				},
			};
		 
			$("#dataKontak").jsGrid({
				width: "100%",
				height: "410px",
				sorting: true,
				paging: true,
				filtering: true,
				autoload: true,
				
				deleteConfirm: "Anda yakin akan menghapus data ini?",
					 
				controller: db,
					 
				fields: [
					{ name: "kontak_id", title:"ID KOntak", type: "text", width: 100 },
					{ name: "kontak_tipe_nama", title:"Tipe ", type: "text", width: 100, validate: "required" },
					{ name: "kontak_kategori_nama", title:"Kategori", type: "text", width: 100, validate: "required" },
					{ name: "kontak_nama", title:"Nama", type: "text", width: 120, validate: "required" },
					{ name: "kontak_nick", title:"Nick", type: "text", width: 100, inserting: false, editing:false },
					{ name: "kontak_alamat", title:"Alamat", type: "text", width: 190, inserting: false, editing:false },
					{ name: "kontak_kota", title:"Kota", type: "text", width: 100, inserting: false, editing:false },
					{ name: "kontak_pos", title:"Pos", type: "text", width: 100, inserting: false, editing:false },
					{ name: "kontak_telp", title:"Telp", type: "text", width: 100, inserting: false, editing:false },
					{ name: "kontak_email", title:"Email", type: "text", width: 100, inserting: false, editing:false },
					{ name: "edit", title:"Action", type: "text", width: 80, validate: "required", align:"center" },
				]

			});
		}

		loadKontakTipe();
		loadData();
	</script>

@endsection