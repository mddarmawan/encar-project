@extends('layouts.app')

@section('content')

	<div class="panel-header-encar">
		<h6 class="left">
			<small style="margin-bottom: 40px">Persediaan</small>
			<br>
		</h6>
		 <h5>Kendaraan</h5>
	</div>

	<!-- Main Content :: START -->
	<div class="content_encar">
		<div class="main-content ">
			<div class="row">
				<div class="col-lg-offset-3 col-lg-4">
					<div class="panel panel-default panel-table">
						<div class="panel-body">
							<!-- Type Data :: START -->
							<div id="dataType">
							
							</div>
							<!-- Type Data :: END -->
						</div> 
					</div>
				</div>
				<div class="col-lg-8">
					<div class="panel panel-default panel-table">
						<div class="panel-body">
							<!-- Variant Data :: START -->
							<div id="dataVariant">
							
							</div>
							<!-- Variant Data :: END -->
						</div> 
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Main Content :: END -->
	
	@section('scripts')

		<script type="text/javascript">

			/**
			 * Clear Form Function
			 *
			 * @param {string}
			 */
			function clearForm(type) {
				switch (type) {
					case "type":	
						//$("#").val('');
					break;

					case "kendaraan":	
						//$("#").val('');
					break;
				}
			}

			/**
			 * Adding Modal
			 *
			 * @param {string}
			 */
			function add(type) {
				Avgrund.show( "#add_" + type );
			}

			/**
			 * Closing Modal
			 *
			 */
			function closeDialog() {
				Avgrund.hide();
			}

		</script>

		@include('modules.kendaraan.data_type')
		@include('modules.kendaraan.data_variant')

	@endsection

@endsection