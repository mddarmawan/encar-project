<div id="add" class="avgrund-popup" >
	<form action="/leasing" method="post">
		{{ csrf_field() }}
		<input id="leasing_id" type="hidden" name="leasing_id">
		<div class="header-modal">
			<a href="#" class="close-modal" onclick="javascript:closeDialog();"><span class="s7-close"></span></a>
			<div class="title-modal">
				Tambah Leasing
				<br>
				<small> Nama Leasing</small>
			</div>
		</div>
		<div class="content-modal">
			<div class="row" style="margin: 0">
				 <div class="col-md-6">
					<table class="info payment" style="border:0;margin-top:0">
						<tr>
							<td width="140px">Nama Leasing</td>
							<td width="10px">:</td>
							<td class="" width="200px"><input name="leasing_nama" type="text" required=""  /></td>
						</tr>
						<tr>
							<td width="140px">Nick</td>
							<td width="10px">:</td>
							<td class="bold" ><input name="leasing_nick" type="text" required=""/></td>
						</tr>
						<tr>
							<td width="140px">No. Telp</td>
							<td width="10px">:</td>
							<td class="bold" width="200px"><input name="leasing_telp" type="text"  required=""/></td>
						</tr>
					</table>
				</div>
				<div class="col-md-6">
					<table class="info payment" style="border:0;margin-top:0">
						<tr>
							<td width="140px" style="vertical-align: top">Alamat</td>
							<td width="10px" style="vertical-align: top">:</td>
							<td class="bold" width="200px"><textarea name="leasing_alamat" class="no-resize"></textarea></td>
						</tr>
						<tr>
							<td width="140px">Kota</td>
							<td width="10px">:</td>
							<td class="bold" width="200px"><input name="leasing_kota" type="text" required=""/></td>
						</tr>
					</table>
				</div>
			</div>
		</div>
		<div class="footer-modal">
			<div style="float: right; margin-right: 30px" >
				<button type="submit" name="submit" class="btn btn-primary"><i class="icon icon-left s7-cloud"></i> Simpan</button>	
			</div>
		</div>
	</form>
</div>