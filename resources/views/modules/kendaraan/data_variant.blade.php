<script type="text/javascript">

	/**
	 * Adding Edit Modal
	 *
	 * @param {number}
	 */
	function edit(id) {
		clearForm();

		$.ajax({
			type: "GET",
			url: "{{url('leasing')}}/" + id
		}).done(function(response){
			var data = JSON.parse(response);

			$("#leasing_id").val(data.leasing_id);
			$("#leasing_nama").val(data.leasing_nama);
			$("#leasing_nick").val(data.leasing_nick);
			$("#leasing_telp").val(data.leasing_telp);
			$("#leasing_alamat").val(data.leasing_alamat);
			$("#leasing_kota").val(data.leasing_kota);
		});

		Avgrund.show( "#edit" );
	}

	/**
	 * On Document Ready Function
	 *
	 * @param {function}
	 */
	$(document).ready(function(){
		App.init();

		var db_variant = {
			loadData: function(filter) {
				return $.ajax({
					type: "GET",
					url: "{{url('api/kendaraan/variant')}}",
					data: filter
				});
			},
		};

		db_variant.status = [
			{
				"status_id": "",
				"status_nama": "",     
			},
			{
				"status_id": 0,
				"status_nama": "<span class='box-span red'>NON. ACTIVED</span>",          
			},
			{
				"status_id": 1,
				"status_nama": "<span class='box-span green'>ACTIVED</span>",           
			},
		];

		$("#dataVariant").jsGrid({
			width: "100%",
			height: "445px",
			sorting: true,
			paging: true,
			filtering: true,
			autoload: true,
			
			deleteConfirm: "Anda yakin akan menghapus data ini?",
				 
			controller: db_variant,
				 
			fields: [
				{ name: "variant_status", title:"Status", type: "select", items: db_variant.status, valueField: "status_id", textField: "status_nama", width: 50, align:"center", filtering:false, editing:false},
				{ name: "variant_serial", title:"ID", type: "text", width: 100, validate: "required" },
				{ name: "variant_nama", title:"Nama", type: "text", width: 130, validate: "required" },
				{ name: "variant_ket", title:"Keterangan", type: "text", width: 170, validate: "required" },
				{ name: "type_poin", title:"Point", type: "number", width: 30, inserting: false, editing:false },
				{ name: "edit", title:"Action", type: "text", width: 80, validate: "required", align:"center" },
			]
		});
	});
</script>