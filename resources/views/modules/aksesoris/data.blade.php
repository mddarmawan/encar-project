@extends('layouts.app')

@section('content')

	@if (!empty(Session::get('message')))
		<script>
			alert('{{ Session::get('message') }}')
		</script>
	@endif

	<div class="panel-header-encar">
		<h6 class="left">
			<small style="margin-bottom: 40px">Master Data</small>
			<br>
		</h6>
		 <h5>Aksesoris</h5>
	</div>	
	<div class="nav-aksi">
		<button onclick="javascript:add();" class="btn-tambah"><i class="icon icon-left s7-plus"> </i> Tambah</button>
	</div>
	<div class="content_encar">
		<div class="main-content ">
			<div class="col-sm-12">
				<div class="panel panel-default panel-table">
					<div class="panel-body">
						<div id="dataAksesoris">

						</div>
					</div> 
				</div>
			</div>
		</div>
	</div>

	<div id="add" class="avgrund-popup" >
		<form id="form" action="/aksesoris" method="post">
			{{ csrf_field() }}
			<input id="form_method" type="hidden" name="_method" value="put">
			<div class="header-modal">
				<a href="#" class="close-modal" onclick="javascript:closeDialog();"><span class="s7-close"></span></a>
				<div class="title-modal">
					Tambah Aksesoris
					<br>
				</div>
			</div>
			<div class="content-modal">	
				<div class="bingkai">
					<div class="label-bingkai">?</div>
					<div class="row" style="padding: 10px;">
						<div class="col-md-6" style="display: inline-block;">
							<table class="table-form">
								<tr>
									<td width="140px">Kode Aksesoris</td>
									<td width="10px">:</td>
									<td> 
										<input id="aksesoris_kode" type="text" placeholder="" name="aksesoris_kode" class="form-control form-control-xs">
									</td>
								</tr>
								<tr>
									<td width="140px">Nama Aksesoris</td>
									<td width="10px">:</td>
									<td> 
										<input id="aksesoris_nama" type="text" placeholder="" name="aksesoris_nama" class="form-control form-control-xs">
									</td>
								</tr>
								<tr>
									<td width="140px">Harga Aksesoris</td>
									<td width="10px">:</td>
									<td> 
										<input id="aksesoris_harga" type="number" placeholder="" name="aksesoris_harga" class="form-control form-control-xs">
									</td>
								</tr>
								<tr>
									<td width="140px">Harga Modal</td>
									<td width="10px">:</td>
									<td> 
										<input id="aksesoris_modal" type="number" placeholder="" name="aksesoris_modal" class="form-control form-control-xs">
									</td>
								</tr>
								<tr>
									<td width="140px">Harga Marketing</td>
									<td width="10px">:</td>
									<td> 
										<input id="aksesoris_marketing" type="number" placeholder="" name="aksesoris_marketing" class="form-control form-control-xs">
									</td>
								</tr>
							</table>
						</div>
						<div class="col-md-6" style="display: inline-block;">
							<table class="table-form">
								<tr>
									<td width="140px">Tipe Kendaraan</td>
									<td width="10px">:</td>
									<td> 
										<select class="form-control custom-select small" id="aksesoris_kendaraan" name="aksesoris_kendaraan">
						
										</select>
									</td>
								</tr>
								<tr>
									<td width="140px">Vendor Aksesoris</td>
									<td width="10px">:</td>
									<td> 
										<select class="form-control custom-select small" id="aksesoris_vendor" name="aksesoris_vendor">
						
										</select>
									</td>
								</tr>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="footer-modal">
				<div style="float: right; margin-right: 30px" >
					<button type="submit" name="submit" class="simpan">Simpan</button>	
				</div>
			</div>
		</form>
	</div>

@endsection

@section('scripts')

	<script type="text/javascript">
		App.init();

		/*
		 * --------------------------------------------------------------------------
		 * Aksi Filter Data
		 * --------------------------------------------------------------------------
		 */
		$("#filter").click(function(e){
			e.preventDefault();
			loadData();
		});

		/*
		 * --------------------------------------------------------------------------
		 * Aksi Reset Data
		 * --------------------------------------------------------------------------
		 */
		$("#reset").click(function(e){
			e.preventDefault();
			$(".tab li").removeClass("active");
			$(".tab li a[data-id='']").parent().addClass("active");
			loadData();
		});

		/*
		 * --------------------------------------------------------------------------
		 * Fungsi Membersihkan Form
		 * --------------------------------------------------------------------------
		 */
		function clearForm() {	
			$("#aksesoris_kode").val('');
			$("#aksesoris_nama").val('');
			$("#aksesoris_kendaraan").val('');
			$("#aksesoris_harga").val('');
			$("#aksesoris_vendor").val('');
			$("#aksesoris_modal").val('');
			$("#aksesoris_marketing").val('');
		}

		/*
		 * --------------------------------------------------------------------------
		 * Fungsi Set Tipe Kendaraan
		 * --------------------------------------------------------------------------
		 */
		function setType(id){
			$.ajax({
			    type: "GET",
			    url: "{{url('api/kendaraan/type')}}"
			}).done(function(data) {
				$("#aksesoris_kendaraan").html("");

				jQuery.each(data, function(i, item) {
					$("#aksesoris_kendaraan").append("<option value='"+ item.type_id +"'>"+ item.type_nama +"</option>");
				});

				$("#aksesoris_kendaraan").val(id);
			});
		}

		/*
		 * --------------------------------------------------------------------------
		 * Fungsi Set Vendor Aksesoris
		 * --------------------------------------------------------------------------
		 */
		function setVendor(){
			$.ajax({
			    type: "GET",
			    url: "{{url('api/kontak/tipe/2')}}"
			}).done(function(data) {
				$("#aksesoris_vendor").html("");
				jQuery.each(data, function(i, item) {
					$("#aksesoris_vendor").append("<option value='0'>"+ item.kontak_nama +"</option>");
				});
			});
		}

		/*
		 * --------------------------------------------------------------------------
		 * Fungsi Close Modal
		 * --------------------------------------------------------------------------
		 */
		function closeDialog() {
			Avgrund.hide( "#add" );
		}

		/*
		 * --------------------------------------------------------------------------
		 * Fungsi Tambah Modal
		 * --------------------------------------------------------------------------
		 */
		function add() {
			clearForm();

			$("#form_method").prop("disabled", "disabled");
			$("#aksesoris_kode").removeAttr("readonly");
			$("#form").attr("action", "/aksesoris/");

			Avgrund.show( "#add" );
			setVendor();
			setType();
		}

		/*
		 * --------------------------------------------------------------------------
		 * Fungsi Modal Edit
		 * --------------------------------------------------------------------------
		 */
		function edit(id) {
			clearForm();

			$("#form_method").removeAttr("disabled");
			$("#aksesoris_kode").prop("readonly", "readonly");

			$.ajax({
				type: "GET",
				url: "{{url('api/aksesoris')}}/" + id
			}).done(function(response){
				var data = response[0];

				$("#form").attr("action", "/aksesoris/" + id);
				$("#aksesoris_kode").val(data.aksesoris_kode);
				$("#aksesoris_nama").val(data.aksesoris_nama);
				$("#aksesoris_kendaraan_nama").val(data.aksesoris_kendaraan);
				$("#aksesoris_harga").val(data.aksesoris_harga);
				$("#aksesoris_modal").val(data.aksesoris_modal);
				$("#aksesoris_marketing").val(data.aksesoris_marketing);

				setVendor();
				setType(data.aksesoris_kendaraan_type);
			});

			Avgrund.show( "#add" );
		}


		/*
		 * --------------------------------------------------------------------------
		 * Fungsi Utama Load Data
		 * --------------------------------------------------------------------------
		 */
		function loadData() {
			var db = {
				loadData: function(filter) {
					return $.ajax({
						type: "GET",
						url: "{{url('api/aksesoris')}}",
						data: filter
					});
				},
			};

			$("#dataAksesoris").jsGrid({
				width: "100%",
				height: "445px",
				sorting: true,
				paging: true,
				filtering: true,
				autoload: true,
				
				deleteConfirm: "Anda yakin akan menghapus data ini?",
					 
				controller: db,
					 
				fields: [
					{ name: "aksesoris_kode", title:"Kode Aksesoris", type: "text", width: 100 },
					{ name: "aksesoris_nama", title:"Nama Aksesoris", type: "text", width: 130, validate: "required" },
					{ name: "aksesoris_kendaraan", title:"Kendaraan", type: "text", width: 130, validate: "required" },
					{ name: "aksesoris_harga", title:"Harga", type: "number", width: 100, validate: "required", align:"right" },
					{ name: "aksesoris_vendor", title:"Vendor", type: "text", width: 100, inserting: false, editing:false },
					{ name: "edit", title:"Action", type: "text", width: 80, validate: "required", align:"center" },
				]
			});
		}

		loadData();
	</script>

@endsection