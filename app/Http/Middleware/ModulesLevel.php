<?php

namespace App\Http\Middleware;

use Closure;
use DB;

class ModulesLevel
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $modules = DB::table("tb_modul")->get();
        $levels = array();
        $levels2 = array();

        foreach ($modules as $modul) {
            if ($modul->modul_parent == 0) {
                $levels[$modul->modul_id]['modul_id'] = $modul->modul_id;
                $levels[$modul->modul_id]['modul_nama'] = $modul->modul_nama;
            } else {
                $levels[$modul->modul_parent]['modul_child'][$modul->modul_id]['modul_id'] = $modul->modul_id;
                $levels[$modul->modul_parent]['modul_child'][$modul->modul_id]['modul_nama'] = $modul->modul_nama;
            }
        }

        echo '<pre>';

        foreach ($levels as $level) {
            if (!empty($level['modul_child'])) {
                foreach ($level['modul_child'] as $child) {
                    if (!empty($levels[$child['modul_id']])) {
                        foreach ($levels[$child['modul_id']]['modul_child'] as $grandchild) {
                            //$levels[$child['modul_id']]['modul_child'][$grandchild['modul_id']]['modul_child']['modul_id'] = $grandchild['modul_id'];
                            //$levels[$child['modul_id']]['modul_child'][$grandchild['modul_id']]['modul_child']['modul_nama'] = $grandchild['modul_nama'];
                            $level['modul_child'][$child['modul_id']]['modul_child'][$grandchild['modul_id']]['modul_id'] = $grandchild['modul_id'];
                            $level['modul_child'][$child['modul_id']]['modul_child'][$grandchild['modul_id']]['modul_nama'] = $grandchild['modul_nama'];
                            //print_r($level['modul_child'][$child['modul_id']]['modul_child']);
                            // echo '<hr>';
                            // print_r($child);
                            // echo '<hr>';
                            // print_r($grandchild);
                            // if (!empty($levels[$level['modul_id']][$grandchild['modul_id']])) {
                            //     print_r($levels[$level['modul_id']][$grandchild['modul_id']]);
                            // }
                            // if (array_key_exists($child['modul_id'], $levels[$level['modul_id']])) {
                            //     $levels['modul_id'] = $levels[$child['modul_id']];
                            // }
                            // print_r($levels['modul_id']);
                            // print_r($levels[$child['modul_id']]);
                            // echo "<h3>".$child['modul_id']."</h3>";
                            // print_r($levels[$child['modul_id']]['modul_child']);
                            // print_r($grandchild);
                            
                        }
                        //echo "<h1>".$child['modul_id']."</h1>";
                    }
                }
            }

            print_r($level);
        }

        // print_r($levels[$child['modul_id']]['modul_child']);

        // if ($data->modul_link != '')
        // {
        //     return redirect($data->modul_link);
        // }
            //print_r($levels);

        // return $next($request);
        echo "<pre>";
        //return print_r($levels);
        return 1;
    }
}
