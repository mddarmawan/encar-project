<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class JabatanController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		return view('modules.jabatan.data');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		$params = array(
			"jabatan_nama"	=> $request->jabatan_nama,
		);

		$data = DB::table("tb_jabatan")->insert($params);

		if ($data) {
			return redirect('/jabatan')->with('message', 'Berhasil menambah.');
		} else {
			return redirect('/jabatan')->with('message', 'Gagal menambah.');
		}
	}
	
	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request)
	{
		$id = $request->jabatan_id;

		$params = array(
			"jabatan_nama"	=> $request->jabatan_nama,
		);

		$data = DB::table("tb_jabatan")->where("jabatan_id", $id)->update($params);

		return redirect('/jabatan')->with('message', 'Berhasil mengedit.');
	}

	/**
	 * Patch the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function patch(Request $request, $id)
	{
		switch ($request->action) {
			case 'Trash':
				$params = array(
					"jabatan_hapus"		=> 1,
				);
				break;

			case 'Recover':
				$params = array(
					"jabatan_hapus"		=> 0,
				);
				break;

			case 'Activate':
				$params = array(
					"jabatan_status"	=> 1,
				);
				break;

			case 'Inactivate':
				$params = array(
					"jabatan_status"	=> 0,
				);
				break;
			
			default:
				$params = array();
				break;
		}

		$data = DB::table("tb_jabatan")->where("jabatan_id", $id)->update($params);

		if ($data) {
			return redirect('/jabatan')->with('message', 'Berhasil mengubah status data.');
		} else {
			return redirect('/jabatan')->with('message', 'Gagal mengubah status data.');
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		$data = DB::table("tb_jabatan")->where("jabatan_id", $id)->delete();

		if ($data) {
			return redirect('/jabatan')->with('message', 'Berhasil menghapus.');
		} else {
			return redirect('/jabatan')->with('message', 'Gagal menghapus.');
		}
	}
}
