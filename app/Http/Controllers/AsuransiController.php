<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;

class AsuransiController extends Controller
{
    function index(){
		$data = DB::table('tb_asuransi')
				->where('asuransi_status', 1)
				->where("asuransi_hapus", 0)
				->get();

			$result = array();
			foreach($data as $r){
			$item = array();
			$item['asuransi_nama'] = $r->asuransi_nama;
			$item['asuransi_telp'] = $r->asuransi_telp;
			$item['asuransi_email'] = $r->asuransi_email;
			$item['asuransi_alamat'] = $r->asuransi_alamat;
			$item['asuransi_keterangan'] = $r->asuransi_keterangan;
			$item['edit'] = "
				<form action='/aksesoris/".$r->asuransi_id."' method='post'>
					<input type='hidden' name='_token' value='".csrf_token()."'>
					<input type='hidden' name='_method' value='delete'>
					<input type='submit' name='submit' value='submit' style='display: none;'>
					<a href='#' class='orange-text' onclick='javascript:edit(".$r->asuransi_id.");'>
						<span class='s7-pen'></span> Update
					</a> ||
					<span class='red-text s7-trash'></span>
					<input style='background-color: transparent; border: none; cursor: pointer;' type='submit' class='red-text' onclick='return confirm(\"Anda yakin ingin menghapus?\"); this.submit();' value='Delete'>
						
					</input>
				</form> ";

			if((!request("asuransi_nama") || strrpos(strtolower($item['asuransi_nama']), strtolower(request("asuransi_nama"))) > -1) &&
				(!request("asuransi_telp") || strrpos(strtolower($item['asuransi_telp']), strtolower(request("asuransi_telp"))) > -1) &&
				(!request("asuransi_email") || strrpos(strtolower($item['asuransi_email']), strtolower(request("asuransi_email"))) > -1) &&
				(!request("asuransi_alamat") || strrpos(strtolower($item['asuransi_alamat']), strtolower(request("asuransi_alamat"))) > -1) &&
				(!request("asuransi_keterangan") || strrpos(strtolower($item['asuransi_keterangan']), strtolower(request("asuransi_keterangan"))) > -1))  
			array_push($result, $item);
		 }

		return json_encode($result);
	}
}
