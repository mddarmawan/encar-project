<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class KontakController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		return view('modules.kontak.data');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		$kontak_tipe = DB::table("tb_kontak_tipe")->where("kontak_tipe_id", $request->kontak_tipe)->first();
		$kontak_last = DB::table("tb_kontak")->where("kontak_tipe", $request->kontak_tipe)->orderBy("kontak_id", "desc")->first();
		$kontak_last_id = ($kontak_last == NULL ? 0 : $kontak_last->kontak_id);
		$kontak_id = sprintf("%03s",  substr($kontak_last_id, 3) + 1);

		$params = array(
			"kontak_id"			=> $kontak_tipe->kontak_tipe_kode . $kontak_id,
			"kontak_tipe"		=> $request->kontak_tipe,
			"kontak_kategori"	=> $request->kontak_kategori,
			"kontak_nama"		=> $request->kontak_nama,
			"kontak_nick"		=> $request->kontak_nick,
			"kontak_alamat"		=> $request->kontak_alamat,
			"kontak_kota"		=> $request->kontak_kota,
			"kontak_pos"		=> $request->kontak_pos,
			"kontak_telp"		=> $request->kontak_telp,
			"kontak_email"		=> $request->kontak_email,
		);

		$data = DB::table("tb_kontak")->insert($params);

		if ($data) {
			return redirect('/kontak')->with('message', 'Berhasil menambah.');
		} else {
			return redirect('/kontak')->with('message', 'Gagal menambah.');
		}
	}
	
	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request)
	{
		$id = $request->kontak_id;

		$params = array(
			"kontak_kategori"	=> $request->kontak_kategori,
			"kontak_nama"		=> $request->kontak_nama,
			"kontak_nick"		=> $request->kontak_nick,
			"kontak_alamat"		=> $request->kontak_alamat,
			"kontak_kota"		=> $request->kontak_kota,
			"kontak_pos"		=> $request->kontak_pos,
			"kontak_telp"		=> $request->kontak_telp,
			"kontak_email"		=> $request->kontak_email,
		);

		$data = DB::table("tb_kontak")->where("kontak_id", $id)->update($params);

		return redirect('/kontak')->with('message', 'Berhasil mengedit.');
	}

	/**
	 * Patch the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function patch(Request $request, $id)
	{
		switch ($request->action) {
			case 'Trash':
				$params = array(
					"kontak_hapus"	=> 1,
				);
				break;

			case 'Recover':
				$params = array(
					"kontak_hapus"	=> 0,
				);
				break;

			case 'Activate':
				$params = array(
					"kontak_status"	=> 1,
				);
				break;

			case 'Inactivate':
				$params = array(
					"kontak_status"	=> 0,
				);
				break;
			
			default:
				$params = array();
				break;
		}

		$data = DB::table("tb_kontak")->where("kontak_id", $id)->update($params);

		if ($data) {
			return redirect('/kontak')->with('message', 'Berhasil mengubah status data.');
		} else {
			return redirect('/kontak')->with('message', 'Gagal mengubah status data.');
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		$data = DB::table("tb_kontak")->where("kontak_id", $id)->delete();

		if ($data) {
			return redirect('/kontak')->with('message', 'Berhasil menghapus.');
		} else {
			return redirect('/kontak')->with('message', 'Gagal menghapus.');
		}
	}
}
