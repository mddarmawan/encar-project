<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class AksesorisController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		return view('modules.aksesoris.data');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		$params = array(
			"aksesoris_vendor" 		=> $request->aksesoris_vendor,
			"aksesoris_kendaraan" 	=> $request->aksesoris_kendaraan,
			"aksesoris_kode" 		=> $request->aksesoris_kode,
			"aksesoris_nama" 		=> $request->aksesoris_nama,
			"aksesoris_harga" 		=> $request->aksesoris_harga,
			"aksesoris_modal" 		=> $request->aksesoris_modal,
			"aksesoris_marketing" 	=> $request->aksesoris_marketing
		);

		$data = DB::table("tb_aksesoris")->insert($params);

		if ($data) {
			return redirect('/aksesoris')->with('message', 'Berhasil menambah.');
		} else {
			return redirect('/aksesoris')->with('message', 'Gagal menambah.');
		}
	}
	
	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		$params = array(
			"aksesoris_vendor" 		=> $request->aksesoris_vendor,
			"aksesoris_kendaraan" 	=> $request->aksesoris_kendaraan,
			"aksesoris_kode" 		=> $request->aksesoris_kode,
			"aksesoris_nama" 		=> $request->aksesoris_nama,
			"aksesoris_harga" 		=> $request->aksesoris_harga,
			"aksesoris_modal" 		=> $request->aksesoris_modal,
			"aksesoris_marketing" 	=> $request->aksesoris_marketing
		);

		$data = DB::table("tb_aksesoris")->where("aksesoris_kode", $id)->update($params);

		return redirect('/aksesoris')->with('message', 'Berhasil mengedit.');
	}

	/**
	 * Patch the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function patch(Request $request, $id)
	{
		switch ($request->action) {
			case 'Trash':
				$params = array(
					"aksesoris_hapus"	=> 1,
				);
				break;

			case 'Recover':
				$params = array(
					"aksesoris_hapus"	=> 0,
				);
				break;

			case 'Activate':
				$params = array(
					"aksesoris_status"	=> 1,
				);
				break;

			case 'Inactivate':
				$params = array(
					"aksesoris_status"	=> 0,
				);
				break;
			
			default:
				$params = array();
				break;
		}

		$data = DB::table("tb_aksesoris")->where("aksesoris_kode", $id)->update($params);

		if ($data) {
			return redirect('/aksesoris')->with('message', 'Berhasil mengubah status data.');
		} else {
			return redirect('/aksesoris')->with('message', 'Gagal mengubah status data.');
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		$data = DB::table("tb_aksesoris")->where("aksesoris_kode", $id)->delete();

		if ($data) {
			return redirect('/aksesoris')->with('message', 'Berhasil menghapus.');
		} else {
			return redirect('/aksesoris')->with('message', 'Gagal menghapus.');
		}
	}
}
