<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class ApiKontakTipeController extends Controller
{
	private $table;
	private $column;

	/**
	 * Menentukan Table dan Kolom yang akan di gunakan selanjutnya.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->table = "tb_kontak_tipe";
		$this->column = "kontak_tipe";
	}

	/**
	 * Menampilkan Data yang terpilih.
	 *
	 * @param int $id
	 * @return void
	 */
	public function show($id)
	{
		$where = array(
			$this->column . "_id" => $id
		);

		return $this->data($where);
	}
	/**
	 * Menampilkan seluruh Data.
	 *
	 * @param array $where
	 * @return void
	 */
	public function data($where = NULL)
	{
		if (!empty($where)) {
			$where = $where;
		} else {
			$where = array();
		}

		$data = DB::table($this->table)
				->where($where)
				->get();

		$result = array();
		foreach($data as $r){
			$item = array();
			$item['kontak_tipe_id'] = $r->kontak_tipe_id;
			$item['kontak_tipe_kode'] = $r->kontak_tipe_kode;
			$item['kontak_tipe_nama'] = ucwords(strtolower($r->kontak_tipe_nama));
			$item['kontak_tipe_tgl'] = date_format(date_create($r->created_at),"d/m/Y");

			if((!request("kontak_tipe_kode") || strrpos(strtolower($item['kontak_tipe_kode']), strtolower(request("kontak_tipe_kode"))) > -1) &&
				(!request("kontak_tipe_nama") || strrpos(strtolower($item['kontak_tipe_nama']), strtolower(request("kontak_tipe_nama"))) > -1))
			{
				$tgl = strtotime(str_replace("/","-",$item['kontak_tipe_tgl']));
				if (request("filter_awal") && request("filter_akhir")){
					$filter_awal = strtotime(str_replace("/","-",request("filter_awal")));
					$filter_akhir = strtotime(str_replace("/","-",request("filter_akhir")));
					if ($filter_awal<=$tgl && $filter_akhir>=$tgl){
						array_push($result, $item);                     
					}
				}else if (request("filter_awal")){
					$filter_awal = strtotime(request("filter_awal"));
					if ($filter_awal<=$tgl){
						array_push($result, $item);                     
					}
				}else if (request("filter_akhir")){
					$filter_akhir = strtotime(request("filter_akhir"));
					if ($filter_akhir>=$tgl){
						array_push($result, $item);                     
					}
				}else{
					array_push($result, $item);
				}
			}
		}

		return json_encode($result);
	}
}
