<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class ApiKontakKategoriController extends Controller
{
	private $table;
	private $column;

	/**
	 * Menentukan Table dan Kolom yang akan di gunakan selanjutnya.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->table = "tb_kontak_kategori";
		$this->column = "kontak_kategori";
	}

	/**
	 * Menampilkan Data yang terpilih.
	 *
	 * @param int $id
	 * @return void
	 */
	public function show($id)
	{
		$where = array(
			$this->column . "_tipe" => $id
		);

		return $this->data($where);
	}
	/**
	 * Menampilkan seluruh Data.
	 *
	 * @param array $where
	 * @return void
	 */
	public function data($where = NULL)
	{
		if (!empty($where)) {
			$where = $where;
		} else {
			$where = array();
		}

		$data = DB::table($this->table)
				->where($where)
				->get();

		$result = array();
		foreach($data as $r){
			$item = array();
			$item['kontak_kategori_id'] = $r->kontak_kategori_id;
			$item['kontak_kategori_tipe'] = $r->kontak_kategori_tipe;
			$item['kontak_kategori_nama'] = $r->kontak_kategori_nama;

			$item['kontak_kategori_tgl'] = date_format(date_create($r->created_at),"d/m/Y");

			if((!request("kontak_kategori_tipe") || strrpos(strtolower($item['kontak_kategori_tipe']), strtolower(request("kontak_kategori_tipe"))) > -1) &&
				(!request("kontak_kategori_nama") || strrpos(strtolower($item['kontak_kategori_nama']), strtolower(request("kontak_kategori_nama"))) > -1))
			{
				$tgl = strtotime(str_replace("/","-",$item['kontak_kategori_tgl']));
				if (request("filter_awal") && request("filter_akhir")) {
					$filter_awal = strtotime(str_replace("/","-",request("filter_awal")));
					$filter_akhir = strtotime(str_replace("/","-",request("filter_akhir")));
					if ($filter_awal<=$tgl && $filter_akhir>=$tgl) {
						array_push($result, $item);                     
					}
				} else if (request("filter_awal")){
					$filter_awal = strtotime(request("filter_awal"));
					if ($filter_awal<=$tgl){
						array_push($result, $item);                     
					}
				} else if (request("filter_akhir")) {
					$filter_akhir = strtotime(request("filter_akhir"));
					if ($filter_akhir>=$tgl){
						array_push($result, $item);                     
					}
				} else {
					array_push($result, $item);
				}
			}
		}

		return json_encode($result);
	}
}
