<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class ApiKendaraanVariantController extends Controller
{
	private $table;
	private $column;

	/**
	 * Menentukan Table dan Kolom yang akan di gunakan selanjutnya.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->table = "tb_variant";
		$this->column = "variant";
	}
	
	/**
	 * Menampilkan Data yang terpilih.
	 *
	 * @param int $id
	 * @return void
	 */
	public function show($id)
	{
		$where = array(
			$this->column . "_id" => $id
		);

		return $this->data($where);
	}

	/**
	 * Menampilkan Data yang tidak di aktifkan.
	 *
	 * @return void
	 */
	public function inactive()
	{
		$where = array(
			$this->column . "_status" => "0"
		);

		return $this->data($where);
	}

	/**
	 * Menampilkan Data yang masuk ke Recycle Bin.
	 *
	 * @return void
	 */
	public function deleted()
	{
		$where = array(
			$this->column . "_hapus" => "1"
		);

		return $this->data($where);
	}

	/**
	 * Menampilkan seluruh Data.
	 *
	 * @param array $where
	 * @return void
	 */
	public function data($where = NULL)
	{
		if (!empty($where)) {
			$where = $where;
		} else {
			$where = array();
		}

		if (!isset($where[$this->column . "_status"])) {
			$where[$this->column . "_status"] = "1";
		}

		$data = DB::table($this->table)
				->where($where)
				->get();

		$result = array();
		foreach($data as $r){
			$item = array();
			$item['variant_id'] = $r->variant_id;
			$item['variant_serial'] = $r->variant_serial;
			$item['variant_type'] = $r->variant_type;
			$item['variant_nama'] = $r->variant_nama;
			$item['variant_ket'] = $r->variant_ket;
			$item['variant_on'] = $r->variant_on;
			$item['variant_off'] = $r->variant_off;
			$item['variant_bbn'] = $r->variant_bbn;
			$item['variant_status'] = $r->variant_status;
			$item['variant_hapus'] = $r->variant_hapus;
			$item['variant_tgl'] = date_format(date_create($r->created_at),"d/m/Y");

			if((!request("variant_serial") || strrpos(strtolower($item['variant_serial']), strtolower(request("variant_serial"))) > -1) &&
				(!request("variant_type") || strrpos(strtolower($item['variant_type']), strtolower(request("variant_type"))) > -1) &&
				(!request("variant_nama") || strrpos(strtolower($item['variant_nama']), strtolower(request("variant_nama"))) > -1) &&
				(!request("variant_ket") || strrpos(strtolower($item['variant_ket']), strtolower(request("variant_ket"))) > -1) &&
				(!request("variant_on") || strrpos(strtolower($item['variant_on']), strtolower(request("variant_on"))) > -1) &&
				(!request("variant_off") || strrpos(strtolower($item['variant_off']), strtolower(request("variant_off"))) > -1) &&
				(!request("variant_bbn") || strrpos(strtolower($item['variant_bbn']), strtolower(request("variant_bbn"))) > -1))
			{
				$tgl = strtotime(str_replace("/","-",$item['variant_tgl']));
				if (request("filter_awal") && request("filter_akhir")){
					$filter_awal = strtotime(str_replace("/","-",request("filter_awal")));
					$filter_akhir = strtotime(str_replace("/","-",request("filter_akhir")));
					if ($filter_awal<=$tgl && $filter_akhir>=$tgl){
						array_push($result, $item);                     
					}
				}else if (request("filter_awal")){
					$filter_awal = strtotime(request("filter_awal"));
					if ($filter_awal<=$tgl){
						array_push($result, $item);                     
					}
				}else if (request("filter_akhir")){
					$filter_akhir = strtotime(request("filter_akhir"));
					if ($filter_akhir>=$tgl){
						array_push($result, $item);                     
					}
				}else{
					array_push($result, $item);
				}
			}
		}

		return json_encode($result);
	}
}
