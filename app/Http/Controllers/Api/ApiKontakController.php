<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class ApiKontakController extends Controller
{
	/**
	 * Variable-variable yang akan di gunakan selanjutnya.
	 *
	 * @var string
	 */
	private $table;
	private $column;
	private $form;
	private $editing = FALSE;
	private $deleted = FALSE;
	private $inactive = FALSE;

	/**
	 * Menentukan Table dan Kolom yang akan di gunakan selanjutnya.
	 *
	 * @return 	void
	 */
	public function __construct()
	{
		$this->table = "tb_kontak";
		$this->column = "kontak";
	}
	
	/**
	 * Menampilkan Data yang terpilih.
	 *
	 * @param 	int $id
	 * @return 	void
	 */
	public function show($id)
	{
		$this->editing = TRUE;

		$where = array(
			$this->column . "_id" => $id
		);

		return $this->data($where);
	}

	/**
	 * Menampilkan Data yang berdasarkan yang di inginkan.
	 *
	 * @param 	int $id
	 * @return 	void
	 */
	public function order($type, $id)
	{
		$column = $this->column . "_" . $type;

		$where = array(
			$column => $id
		);

		return $this->data($where);
	}

	/**
	 * Menampilkan Data yang tidak di aktifkan.
	 *
	 * @return 	void
	 */
	public function inactive()
	{
		$this->inactive = TRUE;

		$where = array(
			$this->column . "_status" => "0"
		);

		return $this->data($where);
	}

	/**
	 * Menampilkan Data yang masuk ke Recycle Bin.
	 *
	 * @return 	void
	 */
	public function deleted()
	{
		$this->deleted = TRUE;

		$where = array(
			$this->column . "_hapus" => "1"
		);

		return $this->data($where);
	}

	/**
	 * Form Aksi untuk Data.
	 *
	 * @return 	string
	 * @param 	int $id
	 */
	public function form($type, $id)
	{
		switch ($type)
		{
			case "normal":
				$this->form = "
					<form action='/kontak/".$id."' method='post'>
						<input type='hidden' name='_token' value='".csrf_token()."'>
						<input type='hidden' name='_method' value='patch'>
						<input type='submit' name='submit' value='submit' style='display: none;'>
						<a href='#' class='orange-text' onclick='javascript:edit(\"".$id."\");'>
							<span class='s7-pen'></span> Update
						</a> ||
						<span class='red-text s7-trash'></span>
						<input name='action' style='background-color: transparent; border: none; cursor: pointer;' type='submit' class='red-text' onclick='return confirm(\"Anda yakin ingin menghapus?\"); this.submit();' value='Trash'>		
						</input>
					</form>
					<form action='/kontak/".$id."' method='post'>
						<input type='hidden' name='_token' value='".csrf_token()."'>
						<input type='hidden' name='_method' value='patch'>
						<input type='submit' name='submit' value='submit' style='display: none;'>
						<span class='orange-text s7-check'></span>
						<input name='action' style='background-color: transparent; border: none; cursor: pointer;' type='submit' class='orange-text' onclick='return confirm(\"Anda yakin ingin menonaktifkan data?\"); this.submit();' value='Inactivate'>		
						</input>
					</form>";
				break;

			case "deleted":
				$this->form = "
					<form action='/kontak/".$id."' method='post'>
						<input type='hidden' name='_token' value='".csrf_token()."'>
						<input type='hidden' name='_method' value='patch'>
						<input type='submit' name='submit' value='submit' style='display: none;'>
						<span class='orange-text s7-check'></span>
						<input name='action' style='background-color: transparent; border: none; cursor: pointer;' type='submit' class='orange-text' onclick='return confirm(\"Anda yakin ingin mengembalikan data?\"); this.submit();' value='Recover'>		
						</input>
					</form>
					<form action='/kontak/".$id."' method='post'>
						<input type='hidden' name='_token' value='".csrf_token()."'>
						<input type='hidden' name='_method' value='delete'>
						<input type='submit' name='submit' value='submit' style='display: none;'>
						<span class='red-text s7-trash'></span>
						<input name='action' style='background-color: transparent; border: none; cursor: pointer;' type='submit' class='red-text' onclick='return confirm(\"Anda yakin ingin menghapus data secara permanen?\"); this.submit();' value='Delete'>		
						</input>
					</form>";
				break;

			case "inactive":
				$this->form = "
					<form action='/kontak/".$id."' method='post'>
						<input type='hidden' name='_token' value='".csrf_token()."'>
						<input type='hidden' name='_method' value='patch'>
						<input type='submit' name='submit' value='submit' style='display: none;'>
						<span class='orange-text s7-check'></span>
						<input name='action' style='background-color: transparent; border: none; cursor: pointer;' type='submit' class='orange-text' onclick='return confirm(\"Anda yakin ingin mengaktifkan kembali data?\"); this.submit();' value='Activate'>		
						</input>
					</form>
					<form action='/kontak/".$id."' method='post'>
						<input type='hidden' name='_token' value='".csrf_token()."'>
						<input type='hidden' name='_method' value='delete'>
						<input type='submit' name='submit' value='submit' style='display: none;'>
						<span class='red-text s7-trash'></span>
						<input name='action' style='background-color: transparent; border: none; cursor: pointer;' type='submit' class='red-text' onclick='return confirm(\"Anda yakin ingin menghapus data secara permanen?\"); this.submit();' value='Delete'>		
						</input>
					</form>";
				break;

			default:
				$this->form = '';
				break;
		}

		return $this->form;
	}

	/**
	 * Menampilkan seluruh Data.
	 *
	 * @param 	array $where
	 * @return 	void
	 */
	public function data($where = NULL)
	{
		$result = array();

		if (empty($where)) {
			$where = array();
		}

		if (!isset($where[$this->column . "_status"])) {
			$where[$this->column . "_status"] = "1";
		}

		if (!isset($where[$this->column . "_hapus"])) {
			$where[$this->column . "_hapus"] = "0";
		}

		$data = DB::table($this->table)->where($where)->get();

		foreach ($data as $r) {
			$kontak_tipe = DB::table("tb_kontak_tipe")->where("kontak_tipe_id", $r->kontak_tipe)->first();
			$kontak_kategori = DB::table("tb_kontak_kategori")->where("kontak_kategori_id", $r->kontak_kategori)->first();

			$item = array(
				"kontak_id"				=> $r->kontak_id,
				"kontak_tipe"			=> $r->kontak_tipe,
				"kontak_kategori"		=> $r->kontak_kategori,
				"kontak_tipe_nama"		=> ucwords(strtolower($kontak_tipe->kontak_tipe_nama)),
				"kontak_tipe_kode"		=> $kontak_kategori->kontak_kategori_tipe,
				"kontak_kategori_nama"	=> ucwords(strtolower($kontak_kategori->kontak_kategori_nama)),
				"kontak_nama"			=> $r->kontak_nama,
				"kontak_nick"			=> $r->kontak_nick,
				"kontak_alamat"			=> $r->kontak_alamat,
				"kontak_kota"			=> $r->kontak_kota,
				"kontak_pos"			=> $r->kontak_pos,
				"kontak_telp"			=> $r->kontak_telp,
				"kontak_email"			=> $r->kontak_email,
				"kontak_hapus"			=> $r->kontak_hapus,
				"kontak_status"			=> $r->kontak_status,
				"kontak_tgl"			=> date_format(date_create($r->created_at),"d/m/Y"),
			);

			if (!$this->editing) {
				if ($this->deleted) {
					$item['edit'] = $this->form("deleted", $r->kontak_id);
				} else if ($this->inactive) {
					$item['edit'] = $this->form("inactive", $r->kontak_id);
				} else {
					$item['edit'] = $this->form("normal", $r->kontak_id);
				}
			}

			if (
				(!request("kontak_tipe") || strrpos(strtolower($item['kontak_tipe']), strtolower(request("kontak_tipe"))) > -1) &&
				(!request("kontak_kategori") || strrpos(strtolower($item['kontak_kategori']), strtolower(request("kontak_kategori"))) > -1) &&
				(!request("kontak_nama") || strrpos(strtolower($item['kontak_nama']), strtolower(request("kontak_nama"))) > -1) &&
				(!request("kontak_nick") || strrpos(strtolower($item['kontak_nick']), strtolower(request("kontak_nick"))) > -1) &&
				(!request("kontak_alamat") || strrpos(strtolower($item['kontak_alamat']), strtolower(request("kontak_alamat"))) > -1) &&
				(!request("kontak_kota") || strrpos(strtolower($item['kontak_kota']), strtolower(request("kontak_kota"))) > -1) &&
				(!request("kontak_pos") || strrpos(strtolower($item['kontak_pos']), strtolower(request("kontak_pos"))) > -1) &&
				(!request("kontak_telp") || strrpos(strtolower($item['kontak_telp']), strtolower(request("kontak_telp"))) > -1) &&
				(!request("kontak_email") || strrpos(strtolower($item['kontak_email']), strtolower(request("kontak_email"))) > -1)
			)
			{
				$tgl = strtotime(str_replace("/","-",$item['kontak_tgl']));

				if (request("filter_awal") && request("filter_akhir")) {
					$filter_awal = strtotime(str_replace("/","-",request("filter_awal")));
					$filter_akhir = strtotime(str_replace("/","-",request("filter_akhir")));

					if ($filter_awal<=$tgl && $filter_akhir>=$tgl){
						array_push($result, $item);                     
					}
				} else if (request("filter_awal")) { 
					$filter_awal = strtotime(request("filter_awal"));

					if ($filter_awal<=$tgl){
						array_push($result, $item);                     
					}
				} else if (request("filter_akhir")) {
					$filter_akhir = strtotime(request("filter_akhir"));

					if ($filter_akhir>=$tgl){
						array_push($result, $item);                     
					}
				} else {
					array_push($result, $item);
				}
			}
		}

		return json_encode($result);
	}
}
