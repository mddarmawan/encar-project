<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class ApiAksesorisController extends Controller
{
	/**
	 * Variable-variable yang akan di gunakan selanjutnya.
	 *
	 * @var string
	 */
	private $table;
	private $column;
	private $form;
	private $editing = FALSE;
	private $deleted = FALSE;
	private $inactive = FALSE;

	/**
	 * Menentukan Table dan Kolom yang akan di gunakan selanjutnya.
	 *
	 * @return 	void
	 */
	public function __construct()
	{
		$this->table = "tb_aksesoris";
		$this->column = "aksesoris";
	}
	
	/**
	 * Menampilkan Data yang terpilih.
	 *
	 * @param 	int $id
	 * @return 	void
	 */
	public function show($id)
	{
		$this->editing = TRUE;

		$where = array(
			$this->column . "_kode" => $id
		);

		return $this->data($where);
	}

	/**
	 * Menampilkan Data yang tidak di aktifkan.
	 *
	 * @return 	void
	 */
	public function inactive()
	{
		$this->inactive = TRUE;

		$where = array(
			$this->column . "_status" => "0"
		);

		return $this->data($where);
	}

	/**
	 * Menampilkan Data yang masuk ke Recycle Bin.
	 *
	 * @return 	void
	 */
	public function deleted()
	{
		$this->deleted = TRUE;

		$where = array(
			$this->column . "_hapus" => "1"
		);

		return $this->data($where);
	}

	/**
	 * Form Aksi untuk Data.
	 *
	 * @return 	string
	 * @param 	int $id
	 */
	public function form($type, $id)
	{
		switch ($type)
		{
			case "normal":
				$this->form = "
					<form action='/aksesoris/".$id."' method='post'>
						<input type='hidden' name='_token' value='".csrf_token()."'>
						<input type='hidden' name='_method' value='patch'>
						<input type='submit' name='submit' value='submit' style='display: none;'>
						<a href='#' class='orange-text' onclick='javascript:edit(\"".$id."\");'>
							<span class='s7-pen'></span> Update
						</a> ||
						<span class='red-text s7-trash'></span>
						<input name='action' style='background-color: transparent; border: none; cursor: pointer;' type='submit' class='red-text' onclick='return confirm(\"Anda yakin ingin menghapus?\"); this.submit();' value='Trash'>		
						</input>
					</form>
					<form action='/aksesoris/".$id."' method='post'>
						<input type='hidden' name='_token' value='".csrf_token()."'>
						<input type='hidden' name='_method' value='patch'>
						<input type='submit' name='submit' value='submit' style='display: none;'>
						<span class='orange-text s7-check'></span>
						<input name='action' style='background-color: transparent; border: none; cursor: pointer;' type='submit' class='orange-text' onclick='return confirm(\"Anda yakin ingin menonaktifkan data?\"); this.submit();' value='Inactivate'>		
						</input>
					</form>";
				break;

			case "deleted":
				$this->form = "
					<form action='/aksesoris/".$id."' method='post'>
						<input type='hidden' name='_token' value='".csrf_token()."'>
						<input type='hidden' name='_method' value='patch'>
						<input type='submit' name='submit' value='submit' style='display: none;'>
						<span class='orange-text s7-check'></span>
						<input name='action' style='background-color: transparent; border: none; cursor: pointer;' type='submit' class='orange-text' onclick='return confirm(\"Anda yakin ingin mengembalikan data?\"); this.submit();' value='Recover'>		
						</input>
					</form>
					<form action='/aksesoris/".$id."' method='post'>
						<input type='hidden' name='_token' value='".csrf_token()."'>
						<input type='hidden' name='_method' value='delete'>
						<input type='submit' name='submit' value='submit' style='display: none;'>
						<span class='red-text s7-trash'></span>
						<input name='action' style='background-color: transparent; border: none; cursor: pointer;' type='submit' class='red-text' onclick='return confirm(\"Anda yakin ingin menghapus data secara permanen?\"); this.submit();' value='Delete'>		
						</input>
					</form>";
				break;

			case "inactive":
				$this->form = "
					<form action='/aksesoris/".$id."' method='post'>
						<input type='hidden' name='_token' value='".csrf_token()."'>
						<input type='hidden' name='_method' value='patch'>
						<input type='submit' name='submit' value='submit' style='display: none;'>
						<span class='orange-text s7-check'></span>
						<input name='action' style='background-color: transparent; border: none; cursor: pointer;' type='submit' class='orange-text' onclick='return confirm(\"Anda yakin ingin mengaktifkan kembali data?\"); this.submit();' value='Activate'>		
						</input>
					</form>
					<form action='/aksesoris/".$id."' method='post'>
						<input type='hidden' name='_token' value='".csrf_token()."'>
						<input type='hidden' name='_method' value='delete'>
						<input type='submit' name='submit' value='submit' style='display: none;'>
						<span class='red-text s7-trash'></span>
						<input name='action' style='background-color: transparent; border: none; cursor: pointer;' type='submit' class='red-text' onclick='return confirm(\"Anda yakin ingin menghapus data secara permanen?\"); this.submit();' value='Delete'>		
						</input>
					</form>";
				break;

			default:
				$this->form = '';
				break;
		}

		return $this->form;
	}

	/**
	 * Menampilkan seluruh Data.
	 *
	 * @param 	array $where
	 * @return 	void
	 */
	public function data($where = NULL)
	{
		$result = array();

		if (empty($where)) {
			$where = array();
		}

		if (!isset($where[$this->column . "_status"])) {
			$where[$this->column . "_status"] = "1";
		}

		if (!isset($where[$this->column . "_hapus"])) {
			$where[$this->column . "_hapus"] = "0";
		}

		$data = DB::table($this->table)->where($where)->get();

		foreach ($data as $r) {
			$type = DB::table("tb_type")->where("type_id", $r->aksesoris_kendaraan)->first();

			$item = array(
				"aksesoris_kode"			=> $r->aksesoris_kode,
				"aksesoris_nama"			=> $r->aksesoris_nama,
				"aksesoris_kendaraan_nama"	=> $type->type_nama,
				"aksesoris_kendaraan_type"	=> $type->type_id,
				"aksesoris_harga"			=> $r->aksesoris_harga,
				"aksesoris_modal"			=> $r->aksesoris_modal,
				"aksesoris_marketing"		=> $r->aksesoris_marketing,
				"aksesoris_vendor"			=> $r->aksesoris_vendor,
				"aksesoris_status"			=> $r->aksesoris_status,
				"aksesoris_tgl"				=> date_format(date_create($r->created_at),"d/m/Y"),
			);

			if (!$this->editing) {
				if ($this->deleted) {
					$item['edit'] = $this->form("deleted", $r->aksesoris_kode);
				} else if ($this->inactive) {
					$item['edit'] = $this->form("inactive", $r->aksesoris_kode);
				} else {
					$item['edit'] = $this->form("normal", $r->aksesoris_kode);
				}
			}

			if (
				(!request("aksesoris_kode") || strrpos(strtolower($item['aksesoris_kode']), strtolower(request("aksesoris_kode"))) > -1) &&
				(!request("aksesoris_nama") || strrpos(strtolower($item['aksesoris_nama']), strtolower(request("aksesoris_nama"))) > -1) &&
				(!request("aksesoris_kendaraan") || strrpos(strtolower($item['aksesoris_kendaraan']), strtolower(request("aksesoris_kendaraan"))) > -1) &&
				(!request("aksesoris_harga") || strrpos(strtolower($item['aksesoris_harga']), strtolower(request("aksesoris_harga"))) > -1) &&
				(!request("aksesoris_vendor") || strrpos(strtolower($item['aksesoris_vendor']), strtolower(request("aksesoris_vendor"))) > -1) &&
				(!request("aksesoris_tgl") || strrpos(strtolower($item['aksesoris_tgl']), strtolower(request("aksesoris_tgl"))) > -1)
			)
			{
				$tgl = strtotime(str_replace("/","-",$item['aksesoris_tgl']));

				if (request("filter_awal") && request("filter_akhir")) {
					$filter_awal = strtotime(str_replace("/","-",request("filter_awal")));
					$filter_akhir = strtotime(str_replace("/","-",request("filter_akhir")));

					if ($filter_awal<=$tgl && $filter_akhir>=$tgl){
						array_push($result, $item);
					}
				} else if (request("filter_awal")) { 
					$filter_awal = strtotime(request("filter_awal"));

					if ($filter_awal<=$tgl){
						array_push($result, $item);
					}
				} else if (request("filter_akhir")) {
					$filter_akhir = strtotime(request("filter_akhir"));

					if ($filter_akhir>=$tgl){
						array_push($result, $item);
					}
				} else {
					array_push($result, $item);
				}
			}
		}

		return json_encode($result);
	}
}
