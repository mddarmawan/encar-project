<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// /*
// |--------------------------------------------------------------------------
// | API Base
// |--------------------------------------------------------------------------
// */
// Route::get('BASE/deleted',		'Api\ApiBaseController@deleted');
// Route::get('BASE/inactive',		'Api\ApiBaseController@inactive');
// Route::get('BASE/{id}',			'Api\ApiBaseController@show');
// Route::get('BASE',				'Api\ApiBaseController@data');

/*
|--------------------------------------------------------------------------
| API Kontak
|--------------------------------------------------------------------------
*/
Route::get('kontak/deleted',		'Api\ApiKontakController@deleted');
Route::get('kontak/inactive',		'Api\ApiKontakController@inactive');
Route::get('kontak/{type}/{id}',	'Api\ApiKontakController@order');
Route::get('kontak/{id}',			'Api\ApiKontakController@show');
Route::get('kontak',				'Api\ApiKontakController@data');

/*
|--------------------------------------------------------------------------
| API Kontak Tipe
|--------------------------------------------------------------------------
*/
Route::get('kontakTipe/{id}',		'Api\ApiKontakTipeController@show');
Route::get('kontakTipe',			'Api\ApiKontakTipeController@data');

/*
|--------------------------------------------------------------------------
| API Kontak Kategori
|--------------------------------------------------------------------------
*/
Route::get('kontakKategori/{id}',	'Api\ApiKontakKategoriController@show');
Route::get('kontakKategori',		'Api\ApiKontakKategoriController@data');

/*
|--------------------------------------------------------------------------
| API Aksesoris
|--------------------------------------------------------------------------
*/
Route::get('aksesoris/deleted',		'Api\ApiAksesorisController@deleted');
Route::get('aksesoris/inactive',	'Api\ApiAksesorisController@inactive');
Route::get('aksesoris/{id}',		'Api\ApiAksesorisController@show');
Route::get('aksesoris',				'Api\ApiAksesorisController@data');

/*
|--------------------------------------------------------------------------
| API Team
|--------------------------------------------------------------------------
*/
Route::get('team/deleted',		'Api\ApiTeamController@deleted');
Route::get('team/inactive',		'Api\ApiTeamController@inactive');
Route::get('team/{id}',			'Api\ApiTeamController@show');
Route::get('team',				'Api\ApiTeamController@data');

/*
|--------------------------------------------------------------------------
| API Jabatan
|--------------------------------------------------------------------------
*/
Route::get('jabatan/deleted',		'Api\ApiJabatanController@deleted');
Route::get('jabatan/inactive',		'Api\ApiJabatanController@inactive');
Route::get('jabatan/{id}',			'Api\ApiJabatanController@show');
Route::get('jabatan',				'Api\ApiJabatanController@data');

/*
|--------------------------------------------------------------------------
| API Karyawan
|--------------------------------------------------------------------------
*/
Route::get('karyawan/deleted',		'Api\ApiKaryawanController@deleted');
Route::get('karyawan/inactive',		'Api\ApiKaryawanController@inactive');
Route::get('karyawan/{id}',			'Api\ApiKaryawanController@show');
Route::get('karyawan',				'Api\ApiKaryawanController@data');




/*
|--------------------------------------------------------------------------
| API Kendaraan Type
|--------------------------------------------------------------------------
*/
Route::get('kendaraan/type/deleted',	'Api\ApiKendaraanTypeController@deleted');
Route::get('kendaraan/type/inactive',	'Api\ApiKendaraanTypeController@inactive');
Route::get('kendaraan/type/{id}',		'Api\ApiKendaraanTypeController@show');
Route::get('kendaraan/type',			'Api\ApiKendaraanTypeController@data');

/*
|--------------------------------------------------------------------------
| API Kendaraan Variant
|--------------------------------------------------------------------------
*/
Route::get('kendaraan/variant/deleted',	'Api\ApiKendaraanVariantController@deleted');
Route::get('kendaraan/variant/inactive','Api\ApiKendaraanVariantController@inactive');
Route::get('kendaraan/variant/{id}',	'Api\ApiKendaraanVariantController@show');
Route::get('kendaraan/variant',			'Api\ApiKendaraanVariantController@data');