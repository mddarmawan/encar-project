<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	$temp = "
		<a href='/kontak'>Kontak</a>
		<br>
		<a href='/aksesoris'>Aksesoris</a>
	";
    return $temp;
});

Route::get('/home', function () {
    return view('index');
});

Route::get('/home', 'HomeController@index')->name('home');

Route::get('asuransi', function () {
    return view('modules.asuransi.data');
});

// /*
// |--------------------------------------------------------------------------
// | CRUD Base
// |--------------------------------------------------------------------------
// */
// Route::patch('/BASE/{id}', 'BaseController@patch');
// Route::delete('/BASE/{id}', 'BaseController@destroy');
// Route::post('/BASE', 'BaseController@store');
// Route::put('/BASE/{id}', 'BaseController@update');
// Route::get('/BASE/{id}', 'BaseController@show');
// Route::get('/BASE', 'BaseController@index');

/*
|--------------------------------------------------------------------------
| CRUD Kontak
|--------------------------------------------------------------------------
*/
Route::patch('/kontak/{id}', 'KontakController@patch');
Route::delete('/kontak/{id}', 'KontakController@destroy');
Route::post('/kontak', 'KontakController@store');
Route::put('/kontak/{id}', 'KontakController@update');
Route::get('/kontak/{id}', 'KontakController@show');
Route::get('/kontak', 'KontakController@index');

/*
|--------------------------------------------------------------------------
| CRUD Aksesoris
|--------------------------------------------------------------------------
*/
Route::patch('/aksesoris/{id}', 'AksesorisController@patch');
Route::delete('/aksesoris/{id}', 'AksesorisController@destroy');
Route::post('/aksesoris', 'AksesorisController@store');
Route::put('/aksesoris/{id}', 'AksesorisController@update');
Route::get('/aksesoris/{id}', 'AksesorisController@show');
Route::get('/aksesoris', 'AksesorisController@index');

/*
|--------------------------------------------------------------------------
| CRUD Team
|--------------------------------------------------------------------------
*/
Route::patch('/team/{id}', 'TeamController@patch');
Route::delete('/team/{id}', 'TeamController@destroy');
Route::post('/team', 'TeamController@store');
Route::put('/team/{id}', 'TeamController@update');
Route::get('/team/{id}', 'TeamController@show');
Route::get('/team', 'TeamController@index');

/*
|--------------------------------------------------------------------------
| CRUD Jabatan
|--------------------------------------------------------------------------
*/
Route::patch('/jabatan/{id}', 'JabatanController@patch');
Route::delete('/jabatan/{id}', 'JabatanController@destroy');
Route::post('/jabatan', 'JabatanController@store');
Route::put('/jabatan/{id}', 'JabatanController@update');
Route::get('/jabatan/{id}', 'JabatanController@show');
Route::get('/jabatan', 'JabatanController@index');

/*
|--------------------------------------------------------------------------
| CRUD Karyawan
|--------------------------------------------------------------------------
*/
Route::patch('/karyawan/{id}', 'KaryawanController@patch');
Route::delete('/karyawan/{id}', 'KaryawanController@destroy');
Route::post('/karyawan', 'KaryawanController@store');
Route::put('/karyawan/{id}', 'KaryawanController@update');
Route::get('/karyawan/{id}', 'KaryawanController@show');
Route::get('/karyawan', 'KaryawanController@index');





/*
|--------------------------------------------------------------------------
| CRUD Kendaraan
|--------------------------------------------------------------------------
*/
Route::get('/kendaraan', 'KendaraanController@index');
Route::delete('/kendaraan/{id}', 'KendaraanController@destroy');
Route::post('/kendaraan', 'KendaraanController@store');
Route::put('/kendaraan', 'KendaraanController@update');
Route::get('/kendaraan/{id}', 'KendaraanController@show');

/*
|--------------------------------------------------------------------------
| CRUD Leasing
|--------------------------------------------------------------------------
*/
Route::get('/leasing', 'LeasingController@index');
Route::delete('/leasing/{id}', 'LeasingController@destroy');
Route::post('/leasing', 'LeasingController@store');
Route::put('/leasing', 'LeasingController@update');
Route::get('/leasing/{id}', 'LeasingController@show');

/*
|--------------------------------------------------------------------------
| CRUD Asuransi
|--------------------------------------------------------------------------
*/
Route::get('api/asuransi', ['middleware' => 'cors','uses'=>'AsuransiController@index']);
Route::get('api/leasing', ['middleware' => 'cors','uses'=>'LeasingController@index']);